# Example Mutation

Example mutation string:
```graphql
mutation {
	a: addCompany(
		input: {name: "Monsters Inc", addresses: {city: "Denver", street: "04 Leroy Trail"}}
	) {
		company {
			id
			name

			addresses {
				city
				street
			}
		}
	}
	b: addCompany(
		input: {name: "Toys R Us", addresses: {city: "Villaba", street: "5121 Riverside Parkway"}}
	) {
		company {
			id
			name

			addresses {
				city
				street
			}
		}
	}
	c: addCompany(
		input: {name: "Good Burger", addresses: {city: "Orion", street: "2 Sunfield Plaza"}}
	) {
		company {
			id
			name

			addresses {
				city
				street
			}
		}
	}
}
```
