namespace Demo;

public class Mutation
{
    public async Task<CreateCompanyPayload> CreateCompanyAsync(
        [Service] IMongoCollection<Company> collection,
        CreateCompanyInput input)
    {
        var company = new Company()
        {
            Name = input.Name,
            Addresses = input.Addresses,
            MainAddress = input.MainAddress
        };

        await collection.InsertOneAsync(company);

        return new CreateCompanyPayload(company);
    }
}
