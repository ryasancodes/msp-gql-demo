namespace Demo;

public class Query
{
    [UsePaging]
    [UseProjection]
    [UseSorting]
    [UseFiltering]
    public IExecutable<Company> GetCompanies([Service] IMongoCollection<Company> collection)
    {
        return collection.AsExecutable();
    }

    [UseFirstOrDefault]
    public IExecutable<Company> GetCompanyById([Service] IMongoCollection<Company> collection, [ID] Guid id)
    {
        return collection.Find(x => x.Id == id).AsExecutable();
    }
}
