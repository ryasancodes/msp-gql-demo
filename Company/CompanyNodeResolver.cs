namespace Demo;

public class CompanyNodeResolver
{
    public Task<Company> ResolveAsync([Service] IMongoCollection<Company> collection, Guid id)
    {
        return collection.Find(x => x.Id == id).FirstOrDefaultAsync();
    }
}
