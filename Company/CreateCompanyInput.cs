namespace Demo;

public record CreateCompanyInput(
    string Name,
    IReadOnlyList<Address> Addresses,
    Address MainAddress
);
