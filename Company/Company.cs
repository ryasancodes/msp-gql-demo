namespace Demo;

[Node(IdField = nameof(Id),
    NodeResolverType = typeof(CompanyNodeResolver),
    NodeResolver = nameof(CompanyNodeResolver.ResolveAsync))]
public class Company
{
    public Guid Id { get; init; }

    public string Name { get; init; }

    public IReadOnlyList<Address> Addresses { get; init; }

    public Address MainAddress { get; init; }
}
