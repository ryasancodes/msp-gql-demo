namespace Demo;

public record CreateCompanyPayload(Company Company);
